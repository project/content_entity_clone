<?php

namespace Drupal\content_entity_clone\Plugin\content_entity_clone\FieldProcessor;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\content_entity_clone\Plugin\FieldProcessorPluginBase;
use Drupal\content_entity_clone\Plugin\FieldProcessorPluginManagerInterface;
use Drupal\layout_builder\Section;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A field processor that copies a layout builder layout.
 *
 * @ContentEntityCloneFieldProcessor(
 *   id = "copy_layout",
 *   label = @Translation("Copy layout"),
 *   description = @Translation("Copy a layout builder layout."),
 *   types = {
 *     "layout_section",
 *   },
 * )
 */
class CopyLayout extends FieldProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The field processor plugin manager.
   *
   * @var \Drupal\content_entity_clone\Plugin\FieldProcessorPluginManagerInterface
   */
  protected $fieldProcessorPluginManager;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * Constructs a CopyLayout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\content_entity_clone\Plugin\FieldProcessorPluginManagerInterface $field_processor_plugin_manager
   *   The field processor plugin manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_generator
   *   The UUID generator.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    FieldProcessorPluginManagerInterface $field_processor_plugin_manager,
    UuidInterface $uuid_generator,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->fieldProcessorPluginManager = $field_processor_plugin_manager;
    $this->uuidGenerator = $uuid_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.content_entity_clone.field_processor'),
      $container->get('uuid'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(FieldItemListInterface $field) {
    if (!$this->entityTypeManager->hasDefinition('block_content')) {
      return;
    }

    /** @var use Drupal\block_content\BlockContentInterface $block_content_storage */
    $block_content_storage = $this->entityTypeManager->getStorage('block_content');

    $cloned_value = [];
    // Clone each section inside of this layout.
    /** @var \Drupal\layout_builder\Plugin\Field\FieldType\LayoutSectionItem $field_item */
    foreach ($field as $delta => $field_item) {
      $original_section = $field_item->section;
      $cloned_section_array = $original_section->toArray();
      // Clone all components inside of this section.
      $cloned_components = [];
      foreach ($original_section->getComponents() as $original_component) {
        $cloned_component_array = $original_component->toArray();
        if (str_starts_with($cloned_component_array['configuration']['id'], 'inline_block:')
          && isset($cloned_component_array['configuration']['provider'])
          && $cloned_component_array['configuration']['provider'] === 'layout_builder'
          && isset($cloned_component_array['configuration']['block_revision_id'])) {
          // This component is a layout builder-provided content block.
          // These blocks are not intended to be shared across layouts.
          // Clone the referenced, original block at the specified revision
          // and use this new, cloned block inside of this cloned component.
          if ($original_block = $block_content_storage->loadRevision($cloned_component_array['configuration']['block_revision_id'])) {
            $cloned_block = $this->cloneBlockContentEntity($original_block);
            $cloned_block->save();
            $cloned_component_array['configuration']['block_id'] = $cloned_block->id();
            $cloned_component_array['configuration']['block_revision_id'] = $cloned_block->getRevisionId();
          }
          else {
            // This component's content block could not be loaded and cloned.
            // We cannot copy this component.
            continue;
          }
        }
        // UUIDs are not intended to be shared.
        // Give this cloned component its own UUID.
        $cloned_component_array['uuid'] = $this->uuidGenerator->generate();
        $cloned_components[$cloned_component_array['uuid']] = $cloned_component_array;
      }
      $cloned_section_array['components'] = $cloned_components;
      $cloned_value[$delta]['section'] = Section::fromArray($cloned_section_array);
    }
    $field->setValue($cloned_value);
  }

  /**
   * Clone a block_content entity using clone config if enabled.
   *
   * @param \Drupal\block_content\BlockContentInterface $original
   *   The block_content entity to clone.
   *
   * @return \Drupal\block_content\BlockContentInterface
   *   The cloned block_content entity (unsaved).
   */
  protected function cloneBlockContentEntity(BlockContentInterface $original) {
    // Determine if a clone configuration exists for this block_content bundle.
    $config = $this
      ->configFactory
      ->get('content_entity_clone.bundle.settings.block_content.' . $original->bundle());
    if ($config->get('enabled')) {
      // Create new, empty block of correct bundle.
      /** @var \Drupal\block_content\BlockContentInterface $clone */
      $clone = $this
        ->blockContentStorage
        ->create(['type' => $original->bundle()]);

      // Clone the original block's field values as configured.
      $fields_config = $config->get('fields');
      foreach ($fields_config as $field_name => $field_config) {
        $cloned_field = clone $original->get($field_name);
        $this
          ->fieldProcessorPluginManager
          ->processField($field_config['id'], $cloned_field);
        $clone->get($field_name)->setValue($cloned_field->getValue());
      }
    }
    else {
      // Fall back to a (deep) core clone.
      $clone = $original->createDuplicate();
    }

    return $clone;
  }

}
