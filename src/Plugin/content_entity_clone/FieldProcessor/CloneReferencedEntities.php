<?php

namespace Drupal\content_entity_clone\Plugin\content_entity_clone\FieldProcessor;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\content_entity_clone\Plugin\FieldProcessorPluginBase;

/**
 * Creates clones of entities in entity reference fields.
 *
 * @ContentEntityCloneFieldProcessor(
 *   id = "clone_referenced_entities",
 *   label = @Translation("Clone referenced entities"),
 *   description = @Translation("Creates clones of entities in entity reference fields."),
 *   types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   },
 * )
 */
class CloneReferencedEntities extends FieldProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function process(FieldItemListInterface $field) {
    $cloned_value = [];
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
    foreach ($field->referencedEntities() as $delta => $original_entity) {
      $cloned_value[$delta]['entity'] = $original_entity->createDuplicate();
    }
    $field->setValue($cloned_value);
  }

}
